import { registerPlugin } from '@capacitor/core';

import type { IDnowPluginPlugin } from './definitions';

const IDnowPlugin = registerPlugin<IDnowPluginPlugin>('IDnowPlugin', {
  web: () => import('./web').then(m => new m.IDnowPluginWeb()),
});

export * from './definitions';
export { IDnowPlugin };
