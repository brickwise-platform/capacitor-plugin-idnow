import { WebPlugin } from '@capacitor/core';

import type { IDnowPluginPlugin, StartVideoIdentOptions, StartVideoIdentResult } from './definitions';

export class IDnowPluginWeb extends WebPlugin implements IDnowPluginPlugin {
  async echo(options: { value: string }): Promise<{ value: string }> {
    console.log('ECHO', options);
    return options;
  }
  async startVideoIdent(options: StartVideoIdentOptions): Promise<StartVideoIdentResult> {
    console.log('StartVideoIdent', options);
    return { success: true, error: false };
  }
}
