export interface StartVideoIdentOptions {
  companyId: string
  transactionToken: string
  apiHost: string
  showVideoOverviewCheck: boolean
  showErrorSuccessScreen: boolean
}

export interface StartVideoIdentResult {
  error: boolean
  info?: string
  success: boolean
}

export interface IDnowPluginPlugin {
  echo(options: { value: string }): Promise<{ value: string }>;
  startVideoIdent(options: StartVideoIdentOptions): Promise<StartVideoIdentResult>;
}
