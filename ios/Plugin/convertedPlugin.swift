//  Converted to Swift 5.4 by Swiftify v5.4.25812 - https://swiftify.com/
///******* IDnowPlugin.swift Cordova Plugin Implementation ******

import Cordova
import IDnowSDK

// The transaction token that should be used for a video identification
private var TRANSACTION_TOKEN_VIDEO_IDENT: String? = nil
// Your company id provided by IDnow
private var COMPANY_ID_VIDEO_IDENT: String? = nil
// Set your company's api host url
private var API_HOST: String? = nil
// The transaction token that should be used for a photo identification
private var TRANSACTION_TOKEN_PHOTO_IDENT: String? = nil
// Your company id provided by IDnow
private var COMPANY_ID_PHOTO_IDENT: String? = nil
var SHOW_ERROR_SUCCESS_SCREEN = true
var SHOW_VIDEO_OVERVIEW_CHECK = true
var keepAliveTimer: Timer?

class IDnowPlugin: CDVPlugin {
    // Member variables go here.
    var idnowController: IDnowController?
    var settings: IDnowSettings?

    //- (void)initIDNow:(CDVInvokedUrlCommand*)command;
    func startVideoIdent(_ command: CDVInvokedUrlCommand?) {
        globalCommand = command

        validateTimer()
        // Set up and customize settings
        settings = IDnowSettings()
        settings?.showErrorSuccessScreen = true
        settings?.showVideoOverviewCheck = true


        COMPANY_ID_VIDEO_IDENT = globalCommand.arguments[0] as? String
        TRANSACTION_TOKEN_VIDEO_IDENT = globalCommand.arguments[1] as? String
        API_HOST = globalCommand.arguments[2] as? String
        SHOW_VIDEO_OVERVIEW_CHECK = Bool(globalCommand.arguments[3])
        SHOW_ERROR_SUCCESS_SCREEN = Bool(globalCommand.arguments[4])



        // Set up IDnowController
        idnowController = IDnowController(settings: settings)

        // Setting dummy dev token and company id -> will instantiate a video identification
        settings?.transactionToken = TRANSACTION_TOKEN_VIDEO_IDENT
        settings?.companyID = COMPANY_ID_VIDEO_IDENT
        settings?.apiHost = API_HOST
        settings?.showVideoOverviewCheck = SHOW_VIDEO_OVERVIEW_CHECK
        settings?.showErrorSuccessScreen = SHOW_ERROR_SUCCESS_SCREEN

        idnowController?.delegate = nil
        weak var weakSelf = self

        // Initialize identification using blocks (alternatively you can set the delegate and implement the IDnowControllerDelegate protocol)
        idnowController?.initialize() { [self] success, error, canceledByUser in
            if success {
                // Start identification using blocks
                weakSelf?.idnowController?.startIdentification(fromViewController: viewController) { [self] success, error, canceledByUser in
                    if success {
                        // If showErrorSuccessScreen (Settings) is disabled
                        // you can show for example an alert to your users.
                    } else {
                        // If showErrorSuccessScreen (Settings) is disabled and error.type == IDnowErrorTypeIdentificationFailed
                        // you can show for example an alert to your users.
                        invalidateTimer()
                        var pluginResult: CDVPluginResult? = nil
                        pluginResult = CDVPluginResult(status: CDVCommandStatus_ERROR, messageAsString: "Video Identification Aborted")
                        commandDelegate.send(pluginResult, callbackId: globalCommand.callbackId)
                    }
                }
            } else if let error = error {
                // Present an alert containing localized error description
                let alertController = UIAlertController(title: "Error", message: error.localizedDescription, preferredStyle: .alert)
                let action = UIAlertAction(title: "Ok", style: .cancel, handler: nil)
                alertController.addAction(action)
                viewController.present(alertController, animated: true)
            }
        }

    }

    func startPhotoIdent(_ command: CDVInvokedUrlCommand?) {

        globalCommand = command


        // Set up and customize settings
        settings = IDnowSettings()
        settings?.showErrorSuccessScreen = true
        settings?.showVideoOverviewCheck = true


        COMPANY_ID_VIDEO_IDENT = globalCommand.arguments[0] as? String
        TRANSACTION_TOKEN_VIDEO_IDENT = globalCommand.arguments[1] as? String
        API_HOST = globalCommand.arguments[2] as? String
        SHOW_VIDEO_OVERVIEW_CHECK = Bool(globalCommand.arguments[3])
        SHOW_ERROR_SUCCESS_SCREEN = Bool(globalCommand.arguments[4])

        // Setting dummy dev token and company id -> will instantiate a photo identification
        settings?.transactionToken = TRANSACTION_TOKEN_PHOTO_IDENT
        settings?.companyID = COMPANY_ID_PHOTO_IDENT
        settings?.apiHost = API_HOST
        settings?.showVideoOverviewCheck = SHOW_VIDEO_OVERVIEW_CHECK
        settings?.showErrorSuccessScreen = SHOW_ERROR_SUCCESS_SCREEN

        // This time we use the delegate instead of blocks (it's your choice)
        idnowController?.delegate = self

        // Initialize identification
        idnowController?.initialize()
    }

    // MARK: - IDnowControllerDelegate -

    func idnowController(_ idnowController: IDnowController?, initializationDidFailWithError error: Error?) {
        // Initialization failed -> Present an alert containing localized error description
        let alertController = UIAlertController(title: "Error", message: error?.localizedDescription, preferredStyle: .alert)
        let action = UIAlertAction(title: "Ok", style: .cancel, handler: nil)
        alertController.addAction(action)
        viewController.present(alertController, animated: true)
    }

    func idnowControllerDidFinishInitializing(_ idnowController: IDnowController?) {
        // Initialization was successfull -> Start identification
        self.idnowController?.startIdentification(fromViewController: self)
    }

    func idnowControllerCanceled(byUser idnowController: IDnowController?) {
        // The identification was canceled by the user.
        // For example the user tapped on the "x"-Button or simply navigates back.
        // Normally you don't have to do anything...
        invalidateTimer()
        var pluginResult: CDVPluginResult? = nil
        pluginResult = CDVPluginResult(status: CDVCommandStatus_ERROR, messageAsString: "Identification Canceled By User")
        commandDelegate.send(pluginResult, callbackId: globalCommand.callbackId)
    }

    func idnowController(_ idnowController: IDnowController?, identificationDidFailWithError error: Error?) {
        // Identification failed
        // If showErrorSuccessScreen (Settings) is disabled and error.type == IDnowErrorTypeIdentificationFailed
        // you can show for example an alert to your users.
        invalidateTimer()
        var pluginResult: CDVPluginResult? = nil
        pluginResult = CDVPluginResult(status: CDVCommandStatus_ERROR, messageAsString: error?.description)
        commandDelegate.send(pluginResult, callbackId: globalCommand.callbackId)
    }

    func idnowControllerDidFinishIdentification(_ idnowController: IDnowController?) {
        // Identification was successfull
        // If showErrorSuccessScreen (Settings) is disabled
        // you can show for example an alert to your users.
        invalidateTimer()
        var pluginResult: CDVPluginResult? = nil
        pluginResult = CDVPluginResult(status: CDVCommandStatus_OK, messageAsString: "Identification Finished")
        commandDelegate.send(pluginResult, callbackId: globalCommand.callbackId)
    }
}