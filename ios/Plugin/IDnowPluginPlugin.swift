import Foundation
import Capacitor
import IDnowSDK

// The transaction token that should be used for a video identification
private var TRANSACTION_TOKEN_VIDEO_IDENT: String? = nil
// Your company id provided by IDnow
private var COMPANY_ID_VIDEO_IDENT: String? = nil
// Set your company's api host url
private var API_HOST: String? = nil
// The transaction token that should be used for a photo identification
private var TRANSACTION_TOKEN_PHOTO_IDENT: String? = nil
// Your company id provided by IDnow
private var COMPANY_ID_PHOTO_IDENT: String? = nil
var SHOW_ERROR_SUCCESS_SCREEN = true
var SHOW_VIDEO_OVERVIEW_CHECK = true
var keepAliveTimer: Timer?

/**
 * Please read the Capacitor iOS Plugin Development Guide
 * here: https://capacitorjs.com/docs/plugins/ios
 */
@objc(IDnowPluginPlugin)
public class IDnowPluginPlugin: CAPPlugin {
    private let implementation = IDnowPlugin()
    
    // Member variables go here.
    var idnowController: IDnowController?
    var settings: IDnowSettings?

    @objc func echo(_ call: CAPPluginCall) {
        let value = call.getString("value") ?? ""
        call.resolve([
            "value": "sixt"
        ])
    }
    
    @obj func startVideoIdent(_ call: CAPPluginCall) {
        validateTimer()
        // Set up and customize settings
        settings = IDnowSettings()
        settings?.showErrorSuccessScreen = true
        settings?.showVideoOverviewCheck = true
        
        COMPANY_ID_VIDEO_IDENT = call.getString("companyId") ?? ""
        TRANSACTION_TOKEN_VIDEO_IDENT = call.getString("transactionToken") ?? ""
        API_HOST = call.getString("apiHost") ?? ""
        SHOW_VIDEO_OVERVIEW_CHECK = call.getBoolean("showVideoOverviewCheck") ?? ""
        SHOW_ERROR_SUCCESS_SCREEN = call.getBoolean("showErrorSuccessScreen") ?? ""
        
        // Set up IDnowController
        idnowController = IDnowController(settings: settings)

        // Setting dummy dev token and company id -> will instantiate a video identification
        settings?.transactionToken = TRANSACTION_TOKEN_VIDEO_IDENT
        settings?.companyID = COMPANY_ID_VIDEO_IDENT
        settings?.apiHost = API_HOST
        settings?.showVideoOverviewCheck = SHOW_VIDEO_OVERVIEW_CHECK
        settings?.showErrorSuccessScreen = SHOW_ERROR_SUCCESS_SCREEN
        settings?.userInterfaceLanguage = "de"

        idnowController?.delegate = nil
        weak var weakSelf = self
        
        // Initialize identification using blocks (alternatively you can set the delegate and implement the IDnowControllerDelegate protocol)
        idnowController?.initialize() { [self] success, error, canceledByUser in
            if success {
                // Start identification using blocks
                weakSelf?.idnowController?.startIdentification(fromViewController: viewController) { [self] success, error, canceledByUser in
                    if success {
                        // If showErrorSuccessScreen (Settings) is disabled
                        // you can show for example an alert to your users.
                        call.resolve([
                            "error": false,
                            "success": true
                        ])
                    } else {
                        // If showErrorSuccessScreen (Settings) is disabled and error.type == IDnowErrorTypeIdentificationFailed
                        // you can show for example an alert to your users.
                        invalidateTimer()
                        var pluginResult: CDVPluginResult? = nil
                        call.resolve([
                            "error": true,
                            "info": pluginResult,
                            "success": false
                        ])
                    }
                }
            } else if let error = error {
                // Present an alert containing localized error description
                let alertController = UIAlertController(title: "Error", message: error.localizedDescription, preferredStyle: .alert)
                let action = UIAlertAction(title: "Ok", style: .cancel, handler: nil)
                alertController.addAction(action)
                viewController.present(alertController, animated: true)
            }
        }
    }
}
