# @capacitor/idnow

Wrapper for IDnowSDK

## Install

```bash
npm install @capacitor/idnow
npx cap sync
```

## API

<docgen-index>

* [`echo(...)`](#echo)
* [`startVideoIdent(...)`](#startvideoident)
* [Interfaces](#interfaces)

</docgen-index>

<docgen-api>
<!--Update the source file JSDoc comments and rerun docgen to update the docs below-->

### echo(...)

```typescript
echo(options: { value: string; }) => any
```

| Param         | Type                            |
| ------------- | ------------------------------- |
| **`options`** | <code>{ value: string; }</code> |

**Returns:** <code>any</code>

--------------------


### startVideoIdent(...)

```typescript
startVideoIdent(options: StartVideoIdentOptions) => any
```

| Param         | Type                                                                      |
| ------------- | ------------------------------------------------------------------------- |
| **`options`** | <code><a href="#startvideoidentoptions">StartVideoIdentOptions</a></code> |

**Returns:** <code>any</code>

--------------------


### Interfaces


#### StartVideoIdentOptions

| Prop                         | Type                 |
| ---------------------------- | -------------------- |
| **`companyId`**              | <code>string</code>  |
| **`transactionToken`**       | <code>string</code>  |
| **`apiHost`**                | <code>string</code>  |
| **`showVideoOverviewCheck`** | <code>boolean</code> |
| **`showErrorSuccessScreen`** | <code>boolean</code> |


#### StartVideoIdentResult

| Prop          | Type                 |
| ------------- | -------------------- |
| **`error`**   | <code>boolean</code> |
| **`info`**    | <code>string</code>  |
| **`success`** | <code>boolean</code> |

</docgen-api>
