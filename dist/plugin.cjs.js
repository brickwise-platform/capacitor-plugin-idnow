'use strict';

Object.defineProperty(exports, '__esModule', { value: true });

var core = require('@capacitor/core');

const IDnowPlugin = core.registerPlugin('IDnowPlugin', {
    web: () => Promise.resolve().then(function () { return web; }).then(m => new m.IDnowPluginWeb()),
});

class IDnowPluginWeb extends core.WebPlugin {
    async echo(options) {
        console.log('ECHO', options);
        return options;
    }
    async startVideoIdent(options) {
        console.log('StartVideoIdent', options);
        return { success: true, error: false };
    }
}

var web = /*#__PURE__*/Object.freeze({
    __proto__: null,
    IDnowPluginWeb: IDnowPluginWeb
});

exports.IDnowPlugin = IDnowPlugin;
//# sourceMappingURL=plugin.cjs.js.map
