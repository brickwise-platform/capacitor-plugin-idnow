var capacitorIDnowPlugin = (function (exports, core) {
    'use strict';

    const IDnowPlugin = core.registerPlugin('IDnowPlugin', {
        web: () => Promise.resolve().then(function () { return web; }).then(m => new m.IDnowPluginWeb()),
    });

    class IDnowPluginWeb extends core.WebPlugin {
        async echo(options) {
            console.log('ECHO', options);
            return options;
        }
        async startVideoIdent(options) {
            console.log('StartVideoIdent', options);
            return { success: true, error: false };
        }
    }

    var web = /*#__PURE__*/Object.freeze({
        __proto__: null,
        IDnowPluginWeb: IDnowPluginWeb
    });

    exports.IDnowPlugin = IDnowPlugin;

    Object.defineProperty(exports, '__esModule', { value: true });

    return exports;

}({}, capacitorExports));
//# sourceMappingURL=plugin.js.map
