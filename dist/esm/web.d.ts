import { WebPlugin } from '@capacitor/core';
import type { IDnowPluginPlugin, StartVideoIdentOptions, StartVideoIdentResult } from './definitions';
export declare class IDnowPluginWeb extends WebPlugin implements IDnowPluginPlugin {
    echo(options: {
        value: string;
    }): Promise<{
        value: string;
    }>;
    startVideoIdent(options: StartVideoIdentOptions): Promise<StartVideoIdentResult>;
}
