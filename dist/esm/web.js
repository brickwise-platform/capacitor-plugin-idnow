import { WebPlugin } from '@capacitor/core';
export class IDnowPluginWeb extends WebPlugin {
    async echo(options) {
        console.log('ECHO', options);
        return options;
    }
    async startVideoIdent(options) {
        console.log('StartVideoIdent', options);
        return { success: true, error: false };
    }
}
//# sourceMappingURL=web.js.map