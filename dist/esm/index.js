import { registerPlugin } from '@capacitor/core';
const IDnowPlugin = registerPlugin('IDnowPlugin', {
    web: () => import('./web').then(m => new m.IDnowPluginWeb()),
});
export * from './definitions';
export { IDnowPlugin };
//# sourceMappingURL=index.js.map